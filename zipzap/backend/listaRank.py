import sqlite3 as sql

con = sql.connect("ViniJrFenomeno.db")
cur = con.cursor()
cur.execute(f'select * from "vini_jr_fenomeno"')
dados = cur.fetchall()

json = {}
for tupla in dados:
    json.update({'id' :tupla[0], 'pontos': tupla[1]})
con.commit()
con.close()