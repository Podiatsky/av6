from backend import app, db, CORS, ViniJrFenomeno, jsonify, os, request, Imagem

with app.app_context():
    db.create_all()
    CORS(app)

    @app.route("/")
    def ola():
        return "backend operante"

    @app.route("/listar_pontos")
    def listar_pontos():
        try:
            lista = db.session.query(ViniJrFenomeno).all()
            lista_retorno = [x.json() for x in lista]
            meujson = {"resultado": "ok"}
            meujson.update({"detalhes": lista_retorno})
            resposta = jsonify(meujson)
            return resposta
        except Exception as e:
            return jsonify({"resultado": "erro", "detalhes": str(e)})


    @app.route('/upload', methods= ['POST'])
    def upload_file():
        f = request.files['files']
        caminho = os.path.dirname(os.path.abspath(__file__))
        com_pasta = os.path.join(caminho, 'imagenDosCria/')
        completo = os.path.join(com_pasta, f.filename)
        f.save(completo)
        return '200'
    
    @app.route("/poe_a_imagem_ae_truta_xD", methods=['post'])
    def incluir_pessoa():
        dados = request.get_json(force=True)
        try:
            nova = Imagem(**dados)
            db.session.add(nova) 
            db.session.commit() 
            return jsonify({"resultado": "ok", "detalhes": "oi"})
        except Exception as e:
            return jsonify({"resultado":"erro", "detalhes":str(e)})



    app.run(debug=True)
