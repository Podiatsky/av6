#
# IMPORTAÇÕES
#

from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
import os

#
# VARIÁVEIS E CONFIGURAÇÕES
#

app = Flask(__name__)

# configurações específicas para o SQLite
caminho = os.path.dirname(os.path.abspath(__file__))
arquivobd = os.path.join(caminho, 'ViniJrFenomeno.db')
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" + arquivobd

db = SQLAlchemy(app)

#
# CLASSES
#

class ViniJrFenomeno(db.Model):
    # atributos da pessoa
    id = db.Column(db.Integer, primary_key=True)
    pontos = db.Column(db.Integer)

    def json(self):
        return {
            "id": self.id,
            "pontos": self.pontos
        }

class Imagem(db.Model):
    # atributos da pessoa
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.Text)
