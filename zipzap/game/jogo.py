import pygame
import sqlite3 as sql
from enfiarImagem import enfiarImagem
from random import randint

pygame.init()

screen = pygame.display.set_mode((640,288))
clock = pygame.time.Clock()

# jogado
class ChaWal01(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load(f'/home/aluno/Downloads/sim2/sim/av6-main/zipzap/backend/imagenDosCria/{enfiarImagem()}')
        self.rect = self.image.get_rect(topleft=(x, y))
        self.velocidade = 0
        self.podePular = True
        self.pontos = 0
    def update(self):
            self.pontos += 1

            if self.rect.y < 140:
                self.rect.y += self.velocidade
                self.velocidade += 1 
            else:
                self.velocidade = 0
 
    def pular(self):
            self.velocidade = -4
            self.rect.y -= 1
            

class Floor(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load('/home/aluno/Downloads/sim2/sim/av6-main/zipzap/imagens/Floor.png')
        self.rect = self.image.get_rect(topleft=(x, y))


class EnemyWal01(pygame.sprite.Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.image = pygame.image.load('/home/aluno/Downloads/sim2/sim/av6-main/zipzap/imagens/EnemyWal01.png')
        self.rect = self.image.get_rect(topleft=(x, y))
        self.velocidade = 1

    def andar(self):
        self.rect.x -= self.velocidade

        if self.rect.x < 0:
            self.kill()
# posições iniciais do player
x = 50
y = 140

# criação do jogador
player = ChaWal01(x, y)
player_group = pygame.sprite.Group()
player_group.add(player)

# criação do obstáculo
platform = Floor(0, 192)
platform_group = pygame.sprite.Group()
platform_group.add(platform)


#criacao das cobra
as_crobra = pygame.sprite.Group()

def mostrarACobra():
    if len(as_crobra) < 3:
            a_cobra = EnemyWal01(400, 164)
            as_crobra.add(a_cobra)

fundo = pygame.image.load('/home/aluno/Downloads/sim2/sim/av6-main/zipzap/imagens/Sky.png')

running = True
while running:
    screen.fill((0,0,0))
    mostrarACobra()
    print(player.velocidade)
    if len(player_group) == 0:
        con = sql.connect("/home/aluno/Downloads/sim2/sim/av6-main/zipzap/backend/ViniJrFenomeno.db")
        cur = con.cursor()
        cur.execute(f'INSERT INTO "vini_jr_fenomeno" (pontos)VALUES ({player.pontos}); ')
        con.commit()
        con.close()
        #pygame.quit()
        running = False

    antes_y = player.rect.y

    

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # captura de eventos do teclado
    pk = pygame.key.get_pressed()

    # verifica se tem que fazer algo
    if pk[pygame.K_a]:
        player.rect.x -= 1
    if pk[pygame.K_d]:
        player.rect.x += 1
    if pk[pygame.K_w]:
        player.rect.y -= 1
    if pk[pygame.K_s]:
        player.rect.y += 1
    if pk[pygame.K_SPACE]:
        print('s')
        player.pular()



    # encostou na cobra?
    toco_na_cobra = pygame.sprite.spritecollide(player, as_crobra, False)
    if toco_na_cobra:
        player.kill()

    # colidiu com o chão?
    collided = pygame.sprite.spritecollide(player, platform_group, False)

    if collided:
        player.rect.y = antes_y -1
    
    for cobra in as_crobra:
        cobra.andar()
    as_crobra.draw(screen)
    player_group.draw(screen)
    platform_group.draw(screen)
    as_crobra.update()
    player_group.update()
    platform_group.update()
  
    #screen.blit(fundo, (0,0))

    pygame.display.flip()

    clock.tick(60)

pygame.quit()
